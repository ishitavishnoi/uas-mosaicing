#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv2/nonfree/features2d.hpp>
#include <stdio.h>

using namespace cv;
using namespace std;

#define DICTIONARY_BUILD 2 // set DICTIONARY_BUILD 1 to do Step 1, otherwise it goes to step 2

int main()
{
int minHessian = 300; //Hessian Threshold
cv::Mat img[8];
img[0]=imread("/home/ishita/first_mos/1.jpg",1);
        img[1]=imread("/home/ishita/first_mos/2.jpg",1);
        img[2]=imread("/home/ishita/first_mos/3.jpg",1);
       img[3]=imread("/home/ishita/first_mos/4.jpg",1);
       img[4]=imread("/home/ishita/first_mos/5.jpg",1);
       img[5]=imread("/home/ishita/first_mos/6.jpg",1);
       img[6]=imread("/home/ishita/first_mos/7.jpg",1);
       img[7]=imread("/home/ishita/first_mos/8.jpg",1);


#if DICTIONARY_BUILD == 1

    //Step 1 - Obtain the set of bags of features.

    //to store the input file names
    char * filename = new char[10];
    //to store the current input image
    Mat input;

    //To store the keypoints that will be extracted by ORB
    vector<KeyPoint> keypoints;
    //To store the SURF descriptor of current image
    Mat descriptor;
    //To store all the descriptors that are extracted from all the images.
    Mat featuresUnclustered(0,0,CV_32F);
    //The ORB feature extractor and descriptor

    //FastFeatureDetector detector;
          OrbFeatureDetector detector( minHessian );
        OrbDescriptorExtractor extractor;// = new cv::OrbDescriptorExtractor;


    //I select 20 (1000/50) images from 1000 images to extract feature descriptors and build the vocabulary
   // for(int f=0;f<5;f++){
        //create the file name of an image
        //sprintf(filename,"home\\ishita\\first_mos\\%i.jpg",f);
        //open the file

        /*cv::Mat img[8];
        img[0]=imread("/home/ishita/first_mos/1.jpg",1);
                img[1]=imread("/home/ishita/first_mos/2.jpg",1);
                img[2]=imread("/home/ishita/first_mos/3.jpg",1);
               img[3]=imread("/home/ishita/first_mos/4.jpg",1);
               img[4]=imread("/home/ishita/first_mos/5.jpg",1);
               img[5]=imread("/home/ishita/first_mos/6.jpg",1);
               img[6]=imread("/home/ishita/first_mos/7.jpg",1);
               img[7]=imread("/home/ishita/first_mos/8.jpg",1);*/

        Mat imagey;
        for(int i=0;i<8;i++)
        {

        input = img[i]; //Load as grayscale
        //detect feature points
        detector.detect(input, keypoints);
        //compute the descriptors for each keypoint
        detector.compute(input, keypoints,descriptor);
        //put the all feature descriptors in a single Mat object
        featuresUnclustered.push_back(descriptor);

        cv::drawKeypoints(input,keypoints,imagey,cv::Scalar(0,0,255),cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);

        cv::imshow("img"+i,imagey);
        //print the percentage
        printf("%i /7 done\n",i);

        cv::waitKey(0);

    }


    //Construct BOWKMeansTrainer
    //the number of bags
    int dictionarySize=200;
    //define Term Criteria
    TermCriteria tc(CV_TERMCRIT_ITER,100,0.001);
    //retries number
    int retries=1;
    //necessary flags
    int flags=KMEANS_RANDOM_CENTERS;
    //Create the BoW (or BoF) trainer
    BOWKMeansTrainer bowTrainer(dictionarySize,tc,retries,flags);
    //convert featuresUnclustered to type CV_32F
    Mat featuresUnclusteredF(featuresUnclustered.rows,featuresUnclustered.cols,CV_32F);
    featuresUnclustered.convertTo(featuresUnclusteredF,CV_32F);
    //cluster the feature vectors
    Mat dictionary=bowTrainer.cluster(featuresUnclusteredF);
    //store the vocabulary
    FileStorage fs("/home/ishita/second_mos/dictionary.yml", FileStorage::WRITE);
    fs << "vocabulary" << dictionary;
    fs.release();



#else
    //Step 2 - Obtain the BoF descriptor for given image/video frame.

    //prepare BOW descriptor extractor from the dictionary
    Mat dictionaryF;
    FileStorage fs("/home/ishita/second_mos/dictionary.yml", FileStorage::READ);
    fs["vocabulary"] >> dictionaryF;
    fs.release();

    //convert to 8bit unsigned format
    Mat dictionary(dictionaryF.rows,dictionaryF.cols,CV_8U);
    dictionaryF.convertTo(dictionary,CV_8U);

    //create a matcher with BruteForce-Hamming distance
    Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create("BruteForce-Hamming");

    //create ORB feature point extracter
    Ptr<FeatureDetector> detector(new OrbFeatureDetector());
    //create ORB descriptor extractor
    Ptr<DescriptorExtractor> extractor(new OrbDescriptorExtractor());
    //create BoF (or BoW) descriptor extractor
    BOWImgDescriptorExtractor bowDE(extractor,matcher);
    //Set the dictionary with the vocabulary we created in the first step
    bowDE.setVocabulary(dictionary);

    //To store the image file name
    char * filename = new char[10];
    //To store the image tag name - only for save the descriptor in a file
    char * imageTag = new char[10];

    //open the file to write the resultant descriptor
    FileStorage fs1("/home/ishita/second_mos/dictionary.yml", FileStorage::WRITE);
   // vector<KeyPoint> keypoints;
    //Mat bowDescriptor;


    //the image file with the location. change it according to your image file location
    sprintf(filename,"/home/ishita/first_mos/1.jpg");
    //read the image
    Mat image=imread(filename,CV_LOAD_IMAGE_GRAYSCALE);
    //To store the keypoints that will be extracted by SURF
    vector<KeyPoint> keypoints;
    //Detect ORB keypoints (or feature points)
    detector->detect(image,keypoints);
    //To store the BoW (or BoF) representation of the image
    Mat bowDescriptor;
    //extract BoW (or BoF) descriptor from given image
    bowDE.compute(image,keypoints,bowDescriptor);
    Mat imagez;
    cv::drawKeypoints(image,keypoints,imagez,cv::Scalar(0,0,255),cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
    cout<<"1 done";
    cv::imshow("1",imagez);
    cv::waitKey(0);


    //prepare the yml (some what similar to xml) file
    sprintf(imageTag,"img1");
    //write the new BoF descriptor to the file
    fs1 << imageTag << bowDescriptor;
    imshow("img1",image);

    //You may use this descriptor for classifying the image.

    //release the file storage
    fs1.release();

    //---------------
    FileStorage fs2("/home/ishita/second_mos/dictionary.yml", FileStorage::APPEND);
    //the image file with the location. change it according to your image file location
    sprintf(filename,"/home/ishita/first_mos/2.jpg");
    //read the image
    Mat image2=imread(filename,CV_LOAD_IMAGE_GRAYSCALE);
    //To store the keypoints that will be extracted by SURF
    vector<KeyPoint> keypoints2;
    //Detect ORB keypoints (or feature points)
    detector->detect(image2,keypoints2);
    //To store the BoW (or BoF) representation of the image
    Mat bowDescriptor2;
    //extract BoW (or BoF) descriptor from given image
    bowDE.compute(image2,keypoints2,bowDescriptor2);
    Mat imagez2;
    cv::drawKeypoints(image2,keypoints2,imagez2,cv::Scalar(0,0,255),cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
    cout<<"2 done";
    cv::imshow("2",imagez2);
    cv::waitKey(0);


    //prepare the yml (some what similar to xml) file
    sprintf(imageTag,"img2");
    //write the new BoF descriptor to the file
    fs2 << imageTag << bowDescriptor2;
    imshow("img2",image2);

    //You may use this descriptor for classifying the image.

    //release the file storage
    fs2.release();

    //--------------
#endif
    printf("\ndone\n");
    return 0;
}
